import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from './../src/app.module';
import { INestApplication } from '@nestjs/common';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (POST)', () => {
    const plansToCalculate = [
      {
        origen: 11,
        destine: 16,
        duration: 20,
        planName: 'FaleMais 30',
      },
      {
        origen: 11,
        destine: 16,
        duration: 20,
        planName: 'FaleMais 300',
      },
    ];
    const expected = {
      calculatedCosts: [
        {
          costWithPlan: 'R$0.00',
          costWithoutPlan: 'R$38.00',
          destine: 16,
          duration: 20,
          origen: 11,
          planName: 'FaleMais 30',
        },
      ],
      errors: ['Plan FaleMais 300 not found'],
    };
    return request(app.getHttpServer())
      .post('/v1/calculateCost')
      .send({ plansToCalculate })
      .expect(201)
      .expect(expected);
  });
});
