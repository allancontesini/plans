export const formatMoney = (amount: number, currency = 'BRL') => {
  return new Intl.NumberFormat('pt-br', {
    style: 'currency',
    currency,
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  }).format(amount);
};
