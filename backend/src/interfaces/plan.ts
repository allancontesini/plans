export interface Plan {
  name: string;
  freeMinutes: number;
}

export interface Fare {
  origen: number;
  destine: number;
  tax: number;
}

export interface PlansToCalculateInput {
  plansToCalculate: PlanInputToCalculate[];
}

export interface PlanInputToCalculate {
  planName: string;
  origen: number;
  destine: number;
  duration: number;
}

export interface CalculateCostResponse {
  calculatedCosts: {
    planName: string;
    origen: number;
    destine: number;
    duration: number;
    costWithPlan: string;
    costWithoutPlan: string;
  }[];
  errors?: string[];
}
