import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();
  });

  describe('calculateCost', () => {
    it('should return "calculatedCosts without any error"', () => {
      const appController = app.get<AppController>(AppController);
      const plansToCalculate = [
        {
          origen: 11,
          destine: 16,
          duration: 20,
          planName: 'FaleMais 30',
        },
        {
          origen: 11,
          destine: 17,
          duration: 80,
          planName: 'FaleMais 60',
        },
        {
          origen: 18,
          destine: 11,
          duration: 200,
          planName: 'FaleMais 120',
        },
      ];
      const expected = {
        calculatedCosts: [
          {
            costWithPlan: 'R$0.00',
            costWithoutPlan: 'R$38.00',
            destine: 16,
            duration: 20,
            origen: 11,
            planName: 'FaleMais 30',
          },
          {
            costWithPlan: 'R$37.40',
            costWithoutPlan: 'R$136.00',
            destine: 17,
            duration: 80,
            origen: 11,
            planName: 'FaleMais 60',
          },
          {
            costWithPlan: 'R$167.20',
            costWithoutPlan: 'R$380.00',
            destine: 11,
            duration: 200,
            origen: 18,
            planName: 'FaleMais 120',
          },
        ],
        errors: [],
      };
      expect(appController.calculateCost({ plansToCalculate })).toStrictEqual(
        expected,
      );
    });

    it('should return "calculated Costs with error when invalid planName"', () => {
      const appController = app.get<AppController>(AppController);
      const plansToCalculate = [
        {
          origen: 11,
          destine: 16,
          duration: 20,
          planName: 'FaleMais 30',
        },
        {
          origen: 11,
          destine: 16,
          duration: 20,
          planName: 'FaleMais 300',
        },
      ];
      const expected = {
        calculatedCosts: [
          {
            costWithPlan: 'R$0.00',
            costWithoutPlan: 'R$38.00',
            destine: 16,
            duration: 20,
            origen: 11,
            planName: 'FaleMais 30',
          },
        ],
        errors: ['Plan FaleMais 300 not found'],
      };
      expect(appController.calculateCost({ plansToCalculate })).toStrictEqual(
        expected,
      );
    });

    it('should return "calculated Costs with error when invalid origen, destine or origen/destine does not exists"', () => {
      const appController = app.get<AppController>(AppController);
      const plansToCalculate = [
        {
          origen: 11,
          destine: 16,
          duration: 20,
          planName: 'FaleMais 30',
        },
        {
          origen: 19,
          destine: 17,
          duration: 80,
          planName: 'FaleMais 60',
        },
        {
          origen: 17,
          destine: 18,
          duration: 80,
          planName: 'FaleMais 60',
        },
      ];
      const expected = {
        calculatedCosts: [
          {
            costWithPlan: 'R$0.00',
            costWithoutPlan: 'R$38.00',
            destine: 16,
            duration: 20,
            origen: 11,
            planName: 'FaleMais 30',
          },
        ],
        errors: [
          'Fare origen 19 to destine 17 not found',
          'Fare origen 17 to destine 18 not found',
        ],
      };
      expect(appController.calculateCost({ plansToCalculate })).toStrictEqual(
        expected,
      );
    });
  });
});
