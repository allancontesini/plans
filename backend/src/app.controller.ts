import { Controller, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import {
  CalculateCostResponse,
  PlansToCalculateInput,
} from './interfaces/plan';

@Controller('v1')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('calculateCost')
  calculateCost(
    @Body() plansToCalculate: PlansToCalculateInput,
  ): CalculateCostResponse {
    return this.appService.calculateCost(plansToCalculate);
  }
}
