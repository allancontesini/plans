import { Injectable } from '@nestjs/common';
import {
  CalculateCostResponse,
  Plan,
  PlanInputToCalculate,
  PlansToCalculateInput,
} from './interfaces/plan';
import { formatMoney } from './utils/utils';

const plans: Plan[] = [
  { name: 'FaleMais 30', freeMinutes: 30 },
  { name: 'FaleMais 60', freeMinutes: 60 },
  { name: 'FaleMais 120', freeMinutes: 120 },
];

const fares = new Map([
  ['11-16', 1.9],
  ['16-11', 2.9],
  ['11-17', 1.7],
  ['17-11', 2.7],
  ['11-18', 0.9],
  ['18-11', 1.9],
]);
@Injectable()
export class AppService {
  calculateCost({
    plansToCalculate,
  }: PlansToCalculateInput): CalculateCostResponse {
    const errors = [];
    const calculatedCosts = plansToCalculate
      .map((plansToCalculate) => this.calculate(plansToCalculate, errors))
      .filter((calculatedCost) => calculatedCost !== undefined);
    return { calculatedCosts, errors };
  }

  private calculate(planToCalculate: PlanInputToCalculate, errors: string[]) {
    const { origen, destine, duration, planName } = planToCalculate;
    const plan = this.getPlan(planName);
    const fare = this.getFare({ origen, destine });
    if (!plan) errors.push(`Plan ${planName} not found`);
    if (!fare)
      errors.push(`Fare origen ${origen} to destine ${destine} not found`);
    if (!plan || !fare) return undefined;
    const difference = duration - plan.freeMinutes;
    const costWithoutPlan = formatMoney(duration * fare);
    if (difference > 0) {
      const costWithPlan = formatMoney(difference * fare * 1.1);
      return {
        origen,
        destine,
        duration,
        planName,
        costWithPlan,
        costWithoutPlan,
      };
    }
    return {
      origen,
      destine,
      duration,
      planName,
      costWithPlan: formatMoney(0),
      costWithoutPlan,
    };
  }

  private getPlan(name: string): Plan {
    const plan = plans.filter((plan) => plan.name === name);
    if (plan.length) return plan[0];
    return undefined;
  }

  private getFare(origenAndDestine: {
    origen: number;
    destine: number;
  }): number {
    const key = `${origenAndDestine.origen.toString()}-${origenAndDestine.destine.toString()}`;
    return fares.get(key);
  }
}
