import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import CostInputContainer from '@/components/costInput';
import CostTable from '@/components/table';
import { CalculatedCosts } from '@/services/calculateCost';
import { TABLE_CELLS, TABLE_HEADERS } from '@/config/config';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
  },
  title: {
    ...theme.typography.button,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(1),
  },
}));

export default function Index() {
  const classes = useStyles();

  const [rows, setRow] = React.useState<CalculatedCosts[]>([]);
  const filterDuplicateRow = (
    row: CalculatedCosts,
    index: number,
    rowsArray: CalculatedCosts[],
  ) =>
    rowsArray.findIndex((t) => JSON.stringify(t) === JSON.stringify(row)) ===
    index;

  const updateRow = (newRows: CalculatedCosts[]) => {
    setRow(newRows.concat(rows).filter(filterDuplicateRow));
  };
  return (
    <>
      <Container className={classes.root} maxWidth="sm">
        <Typography align="center" className={classes.title}>
          Calculadora de valor da ligação.
        </Typography>
      </Container>
      <Container maxWidth="sm">
        <Box my={8}>
          <CostInputContainer updateRow={updateRow} />
        </Box>
      </Container>
      {rows.length > 0 ? (
        <Container maxWidth="md">
          <CostTable rows={rows} headers={TABLE_HEADERS} cells={TABLE_CELLS} />
        </Container>
      ) : undefined}
    </>
  );
}
