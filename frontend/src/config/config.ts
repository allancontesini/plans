export const API_URL =
  process.env.API_URL || `https://plans-gitlab-ci.herokuapp.com`;

export const CALCULATE_COST_RESOURCE =
  process.env.CALCULATE_COST_RESOURCE || `/v1/calculateCost`;

export const TABLE_HEADERS = process.env.TABLE_HEADERS?.split(`,`) || [
  `DDD Origem`,
  `DDD Destino`,
  `Duração da ligação`,
  `Plano`,
  `Com FaleMais`,
  `Sem FaleMais`,
];

export const TABLE_CELLS = process.env.TABLE_CELLS?.split(`,`) || [
  `origen`,
  `destine`,
  `duration`,
  `planName`,
  `costWithPlan`,
  `costWithoutPlan`,
];

export const PLANS = process.env.PLANS?.split(`,`) || [
  `FaleMais 30`,
  `FaleMais 60`,
  `FaleMais 120`,
];
