import { API_URL, CALCULATE_COST_RESOURCE } from '@/config/config';

export interface PlansToCalculateInput {
  plansToCalculate: PlanInputToCalculate[];
}

export interface PlanInputToCalculate {
  planName: string;
  origen: number;
  destine: number;
  duration: number;
}

export interface CalculatedCosts extends PlanInputToCalculate {
  costWithPlan: string;
  costWithoutPlan: string;
}

export interface CalculateCostResponse {
  calculatedCosts: CalculatedCosts[];
  errors?: any[];
}

async function fetchAPI(path, body: any) {
  const headers = { 'Content-Type': `application/json` };
  const res = await fetch(API_URL + path, {
    method: `POST`,
    headers,
    body: JSON.stringify(body),
  });
  return res.json();
}

export async function calculateCost(
  plansToCalculateInput: PlansToCalculateInput,
): Promise<CalculateCostResponse> {
  return fetchAPI(CALCULATE_COST_RESOURCE, plansToCalculateInput);
}
