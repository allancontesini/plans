import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    table: {
      minWidth: 650,
    },
    container: {
      [theme.breakpoints.down(`sm`)]: {
        maxHeight: 440,
        maxWidth: 320,
      },
      [theme.breakpoints.down(`md`)]: {
        maxHeight: 640,
        maxWidth: 820,
      },
      [theme.breakpoints.down(`lg`)]: {
        maxHeight: 920,
        maxWidth: 1020,
      },
    },
  }),
);

type CostTableProps = {
  rows: any[];
  headers: string[];
  cells: string[];
};

export default function CostTable({ rows, headers, cells }: CostTableProps) {
  const classes = useStyles();

  return (
    <TableContainer className={classes.container} component={Paper}>
      <Table stickyHeader aria-label="sticky table" className={classes.table}>
        <TableHead>
          <TableRow>
            {headers.map((header, index) => (
              <TableCell key={index} align="right">
                {header}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, indexRow) => (
            <TableRow key={indexRow}>
              {cells.map((cell, indexCell) => (
                <TableCell key={indexCell} align="right">
                  {row[cell]}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
