import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import {
  Backdrop,
  Button,
  CircularProgress,
  FormHelperText,
  Snackbar,
  TextField,
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import {
  calculateCost,
  CalculatedCosts,
  PlansToCalculateInput,
} from '@/services/calculateCost';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { PLANS } from '@/config/config';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: `#fff`,
  },
}));

type CostInputContainerProps = {
  updateRow: (newRows: CalculatedCosts[]) => void;
};

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function CostInputContainer({
  updateRow,
}: CostInputContainerProps) {
  const classes = useStyles();
  const isSmall = useMediaQuery((theme: any) => theme.breakpoints.down(`sm`));
  const [loading, setLoading] = React.useState(false);
  const handleCloseLoading = () => {
    setLoading(false);
  };
  const handleToggleLoading = () => {
    setLoading(!loading);
  };
  const [snackBar, setSnackBar] = React.useState(false);

  const handleSnackBar = () => {
    setSnackBar(true);
  };

  const handleCloseSnackBar = (
    event?: React.SyntheticEvent,
    reason?: string,
  ) => {
    if (reason === `clickaway`) {
      return;
    }
    setSnackBar(false);
  };

  const getFormValues = (event: any): PlansToCalculateInput => {
    const informationsToCalculateCost = {
      origen: event.target[0].value as number,
      destine: event.target[1].value as number,
      duration: event.target[2].value as number,
    };
    const plansToCalculate = new Array(PLANS.length)
      .fill(informationsToCalculateCost, 0, PLANS.length)
      .map((planToCalculate, index) => ({
        ...planToCalculate,
        planName: PLANS[index],
      }));
    return { plansToCalculate };
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    handleToggleLoading();
    try {
      const plansToCalculate = getFormValues(event);
      await calculateCost(plansToCalculate).then((resp) => {
        updateRow(resp.calculatedCosts);
        if (resp.errors.length) handleSnackBar();
        handleCloseLoading();
      });
    } catch (error) {
      handleCloseLoading();
    }
  };

  return (
    <>
      <CssBaseline />
      <Container maxWidth="sm">
        <form className={classes.root} onSubmit={onSubmit} autoComplete="off">
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={3}
          >
            <Grid item xs={12} sm={6}>
              <TextField
                label="DDD Origem"
                type="number"
                required
                inputProps={{ 'aria-label': `description` }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="DDD Destino"
                type="number"
                required
                inputProps={{ 'aria-label': `description` }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Duração"
                type="number"
                aria-describedby="component-helper-text"
                required
                inputProps={{ 'aria-label': `description` }}
              />
              <FormHelperText id="component-helper-text">
                em minutos
              </FormHelperText>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                variant="contained"
                size={isSmall ? `small` : `medium`}
                type="submit"
                color="primary"
              >
                Calcular Custo
              </Button>
            </Grid>
          </Grid>
        </form>
        <Backdrop className={classes.backdrop} open={loading}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <Snackbar
          open={snackBar}
          autoHideDuration={6000}
          onClose={handleCloseSnackBar}
        >
          <Alert onClose={handleCloseSnackBar} severity="error">
            DDD de origem e destino não suportado
          </Alert>
        </Snackbar>
      </Container>
    </>
  );
}
