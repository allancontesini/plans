### FaleMais

Ambos backend e frontend possuem o `.gitlab-ci.yml` configurado para rodar toda a pipeline de devops desde baixar as dependencias do projeto até o deploy e claro também rodando os testes unitarios, e2e, lint, typecheck, etc.

Como foi dito anteriomente até mesmo o deploy para um ambiente de staging foi configurado no gitlab-ci então ambos backend e frontend podem ser acessados pelas seguintes URL's 'https://plans-front-end.herokuapp.com/' e 'https://plans-gitlab-ci.herokuapp.com/v1/calculateCost'.

OBS: Como foi utilzido apenas as intancias de uso gratuito do heroku tanto o backend quanto o frontend possui um delay até serem provisionados novas instâncias

Caso você queira rodar o backend localmente basta executar os seguintes comandos:

````bash
cd backend
yarn # baixa as dependencias
yarn start # start o projeto
### opcionais
yarn test:e2e ## executa o teste de e2e
yarn test ## executa o teste unitario
````

Para o frontend:
````bash
cd frontend
yarn # baixa as dependencias
yarn dev # start o projeto
````

Escolhi utilizar o nestjs na construção do backend e o nextjs no desenvolvimento do frontend, decidi não utilizar um banco pois facilitou o deploy da aplicação no heroku pois o mesmo possui bastante limitações
no tier gratuito, preferi construir todo o pipeline de devops usando o gitlab ci tanto para o backend quanto para o frontend, existe ainda algumas melhorias que eu posso fazer como por exemplo adicionar mais um job no projeto do frontend que faça analise da aplicação usando o [Lighthouse](https://github.com/GoogleChrome/lighthouse)

Falando em pagespeed segue score da aplicação frontend no mesmo:

<img src="./docs/pagespeed.png" alt="pagespeed" width="850px"/>

